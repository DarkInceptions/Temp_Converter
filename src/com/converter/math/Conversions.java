package com.converter.math;

/**
 * Calculates the conversions.
 * 
 * @author Spencer MaGill (spencermagill@gmail.com)
 * @version 6-30-2016
 */
public class Conversions {
	/**
	 * Conversion factor from Fahrenheit to Celsius.
	 */
	private static final double F_CONVERSION_FACTOR = 5.0 / 9.0;
	/**
	 * Conversion factor from Celsius to Fahrenheit.
	 */
	private static final double C_CONVERSION_FACTOR = 9.0 / 5.0;
	
	/**
	 * Takes degree in Fahrenheit and return Celsius.
	 * @param input double Fahrenheit that needs to be converted
	 * @return double Celsius equivalent
	 */
	public static double fToC(double input) {
		double output = (input - 32) * F_CONVERSION_FACTOR;
		output = Math.round(output * 100.0) / 100.0;
		return output;
	}
	
	/**
	 * Takes degree in Celsius and return Fahrenheit.
	 * @param input double Celsius that needs to be converted
	 * @return double Fahrenheit equivalent
	 */
	public static double cToF(double input) {
		double output = (input * C_CONVERSION_FACTOR) + 32;
		output = Math.round(output * 100.0) / 100.0;
		return output;
	}
}
