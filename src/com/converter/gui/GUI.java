package com.converter.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.converter.math.Conversions;

/**
 * Creates the GUI for the program. Creates frame and adds panel to allow
 * for user to convert between Fahrenheit and Celsius.
 * 
 * @author Spencer MaGill (spencermagill@gmail.com)
 * @version 6-30-2016
 */
public class GUI {
	
	/**
	 * Used to represent unicode degree symbol.
	 */
	private final String DEGREE_SYMBOL = "\u00B0";
	
	private double fIn;
	private double fOut;
	private double cIn;
	private double cOut;
	
	private JFrame frame;
	
	private JPanel conversionPanel;
	private JPanel buttonPanel;
	
	private JButton convert;
	
	private JRadioButton fc;
	private JRadioButton cf;
	
	private ButtonGroup group;
	
	private JLabel label;
	private JLabel ans;
	
	private JTextField textfield;

	/**
	 * Constructor for GUI. Creates objects for the frame, panels,
	 * labels, textfield, and radiobuttons.
	 */
	public GUI() {
		frame = new JFrame();
		conversionPanel = new JPanel();
		buttonPanel = new JPanel();
		convert = new JButton("Convert");
		label = new JLabel("Please select a conversion.");
		ans = new JLabel();
		textfield = new JTextField();
		group = new ButtonGroup();
		fc = new JRadioButton(DEGREE_SYMBOL + "Fahrenheit to " + DEGREE_SYMBOL + "Celsius");
		cf = new JRadioButton(DEGREE_SYMBOL + "Celsius to " + DEGREE_SYMBOL + "Fahrenheit");
	}

	/**
	 * Sets up the frame's size, location and adds panels.
	 */
	public void createFrame() {
		frame.setSize(new Dimension(450, 200));
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setTitle("Temperature Converter");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(buttonPanel, BorderLayout.NORTH);
		frame.getContentPane().add(conversionPanel, BorderLayout.CENTER);
		frame.getContentPane().add(convert, BorderLayout.SOUTH);
		frame.setVisible(true);
	}

	/**
	 * Sets up panel to handle the conversions.
	 */
	public void createConversionPanel() {
		conversionPanel.setLayout(new GridLayout(0,1));
		textfield.setPreferredSize(new Dimension(100, 30));
		
		convert.addActionListener(new ActionListener() {
			/**
			 * Handles when the convert button is pressed.
			 * Depending on radiobutton will use Conversions class to convert
			 * input to different measurement.
			 */
			public void actionPerformed(ActionEvent e) {
				if(fc.isSelected()) {
					fIn = Double.parseDouble(textfield.getText());
					cOut = Conversions.fToC(fIn);
					ans.setText(fIn +" " + DEGREE_SYMBOL + "Fahrenheit is Equal to " + cOut + " " + DEGREE_SYMBOL + "Celsius");
				}
				if(cf.isSelected()) {
					cIn = Double.parseDouble(textfield.getText());
					fOut = Conversions.cToF(cIn);
					ans.setText(cIn +" " + DEGREE_SYMBOL + "Celsius is Equal to " + fOut + " " + DEGREE_SYMBOL + "Fahrenheit");
				}
			}
		});

		conversionPanel.add(label);
		conversionPanel.add(textfield);
		conversionPanel.add(ans);
	}

	/**
	 * Creates panel for the radio buttons.
	 */
	public void createButtonPanel() {
		group.add(fc);
		group.add(cf);

		buttonPanel.setLayout(new GridLayout(0,1));
		buttonPanel.add(fc);
		buttonPanel.add(cf);
		
		fc.addActionListener(new ActionListener() {
			/**
			 * When the the fc button is clicked the panel will clear the textfield
			 * and change a label so user knows to enter a degree in Fahrenheit.
			 */
			public void actionPerformed(ActionEvent e) {
				label.setText("Enter " + DEGREE_SYMBOL + "Fahrenheit:");
				textfield.setText(null);
				ans.setText(null);
			}
		});
		
		cf.addActionListener(new ActionListener() {
			/**
			 * When the the cf button is clicked the panel will clear the textfield
			 * and change a label so user knows to enter a degree in Celsius.
			 */
			public void actionPerformed(ActionEvent e) {
				label.setText("Enter " + DEGREE_SYMBOL + "Celsius:");
				textfield.setText(null);
				ans.setText(null);
			}
		});
	}
}
