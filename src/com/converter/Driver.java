package com.converter;

import com.converter.gui.GUI;

/**
 * Driver program to start program.
 * 
 * @author - Spencer MaGill (spencermagill@gmail.com)
 * @version 6-30-2016
 */
public class Driver {
	
	/**
	 * Main method used to create new GUI instance.
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		GUI g = new GUI();
		g.createFrame();
		g.createConversionPanel();
		g.createButtonPanel();
	}
}
